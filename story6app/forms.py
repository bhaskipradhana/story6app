from django import forms
from .models import story6models

class story6forms(forms.ModelForm):
    status = forms.CharField(label = "")

    class Meta:
        model = story6models
        fields = {'status'}
    
    def __init__(self, *args, **kwargs):
        super(story6forms, self).__init__(*args, **kwargs)
        self.fields["status"].widget.attrs['id'] = "kotak-input"
        
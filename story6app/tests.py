from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from .views import *
from .models import *


#unittest
class story6_test(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_status_using_story6_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_create_status(self):
        activity = story6models.objects.create(status = "tes")
        
        counting_todo = story6models.objects.all().count()
        self.assertEqual(counting_todo, 1)

    def test_view(self):
        handler = resolve('/')
        self.assertEqual(handler.func, index)

    def test_models_string(self):
        obj = story6models.objects.create(status= "tes")
        self.assertEqual(str(obj), "tes")

    def test_after_post(self):
        response = Client().post('/', {'status' : 'tes'})
        self.assertEqual(response.status_code, 200)

#functionaltest

class functionaltest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        # self.browser.implicitly_wait(25)
        super(functionaltest, self).setUp()
    
    def tearDown(self):
        self.browser.quit()
        super(functionaltest, self).tearDown()

    
    def test_input(self):
        selenium = self.browser

        selenium.get(self.live_server_url)

        time.sleep(3)

        status = selenium.find_element_by_id('kotak-input')

        submit = selenium.find_element_by_id('submit')

        time.sleep(3)
        status.send_keys('Tes')

        time.sleep(3)
        submit.click()

        time.sleep(3)
        self.assertIn('Tes', selenium.page_source)
        
    
# Create your tests here.

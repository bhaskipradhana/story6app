from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import story6models
from .forms import story6forms
# Create your views here.

def index(request):
    if request.method == 'POST':
        formulir = story6forms(request.POST)
        if formulir.is_valid():
            formulir.save()
            obj = story6models.objects.all().order_by('tanggal').reverse()
            arg = {
                'formulir': formulir,
                'obj' : obj
            }
            return render(request, 'story6.html', arg)
    else:
        formulir = story6forms()
        obj = story6models.objects.all().order_by('tanggal').reverse()
        arg = {
            'formulir': formulir,
            'obj' : obj
        }
        return render(request, 'story6.html', arg)


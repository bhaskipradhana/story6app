# Story 6

[![Pipeline](https://gitlab.com/bhaskipradhana/story6app/badges/master/pipeline.svg)](https://gitlab.com/bhaskipradhana/stor6app/pipelines)
[![coverage report](https://gitlab.com/bhaskipradhana/story6app/badges/master/coverage.svg)](https://gitlab.com/bhaskipradhana/story6app/commits/master)


Bhaskoro's PPW Lab 6 Project 

## URL

This lab projects can be accessed from [https://bhaski-story6.herokuapp.com](https://bhaski-story6.herokuapp.com)

## Authors

* **Bhaskoro Siddhi Pradhana** - [bhaskipradhana](https://gitlab.com/bhaskipradhana)
